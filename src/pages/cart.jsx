import { useSelector } from "react-redux";
import ProductList from "../components/cards/product-list";
import Form from "../components/form";

export function Cart(props){
    const cartItems = useSelector(state => state.productsReducer.cartItems);
    return (
        <>
            <h1 style={{textAlign: 'center', color: '#007eb9'}}>Cart</h1>
            {cartItems.length > 0 ? (
            <ProductList
                products={props.products}
                cartItems={props.cartItems}
                // cartItems={cartItems}
                favourites={props.favourites}
                onAddProductToCart={props.onAddProductToCart}
                onRemoveProductFromCart={props.onRemoveProductFromCart}
                onAddProductToFavs={props.onAddProductToFavs}
                onRemoveProductFromFavs={props.onRemoveProductFromFavs}  
            />
                ) : (
                null)
            }
            {cartItems.length > 0 ? (
                <Form />
                ) : (
                <p style={{ textAlign: 'center', color: '#818181', fontSize: '18px', margin: 0}}>
                    Your cart is empty.
                </p>
            )}
        </>
    )
}