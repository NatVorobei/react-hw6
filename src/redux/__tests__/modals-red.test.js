import { modalsReducer } from "../reducers/modals";
import { openModal, closeModal } from "../action/modals";

test('should handle openModal action', () => {
  const initialState = {
    isModalOpened: false,
    modalData: {},
  };

  const modalData = { title: 'Modal Title', content: 'Modal Data' };

  const expectedState = {
    isModalOpened: true,
    modalData: modalData,
  };

  const newState = modalsReducer(initialState, openModal(modalData));
  expect(newState).toEqual(expectedState);
});

test('should handle closeModal action', () => {
  const initialState = {
    isModalOpened: true,
    modalData: { title: 'Modal Title', content: 'Modal Data' },
  };

  const expectedState = {
    isModalOpened: false,
    modalData: {},
  };

  const newState = modalsReducer(initialState, closeModal());
  expect(newState).toEqual(expectedState);
});

