import {productsReducer} from '../reducers/products';
import {addToCart, addToFavourites, removeFromCart, removeFromFavourites} from '../action/products';

describe('productsReducer', () => {
    const initialState = {
      products: [],
      cartItems: [],
      favourites: [],
    };

    it('should handle addToCart action', () => {
        const productId = 1;
        const expectedState = {
            ...initialState,
            cartItems: [productId],
        };
        expect(productsReducer(initialState, addToCart(productId))).toEqual(expectedState);
    });
  
    it('should handle removeFromCart action', () => {
        const productId = 1;
        const state = {
            ...initialState,
            cartItems: [productId],
        };
        const expectedState = {
            ...initialState,
            cartItems: [],
        };
        expect(productsReducer(state, removeFromCart(productId))).toEqual(expectedState);
    });
  
    it('should handle addToFavourites action', () => {
        const productId = 1;
        const expectedState = {
            ...initialState,
            favourites: [productId],
        };
        expect(productsReducer(initialState, addToFavourites(productId))).toEqual(expectedState);
    });
  
    it('should handle removeFromFavourites action', () => {
        const productId = 1;
        const state = {
            ...initialState,
            favourites: [productId],
        };
        const expectedState = {
            ...initialState,
            favourites: [],
        };
        expect(productsReducer(state, removeFromFavourites(productId))).toEqual(expectedState);
    });
});