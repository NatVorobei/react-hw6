import React from 'react';
import Button from '../buttons';
import { StarNotSolid, StarSolid } from '../icons';

export default function ProductTable(props) {
  const {
    name,
    price,
    image,
    sku,
    color,
    onAddToCart,
    onRemoveFromToCart,
    onAddToFavourite,
    onRemoveFromFavourite,
    isAddedToCart,
    isAddedToFavourites
  } = props;

  return (
    <tr id={'sku' + sku} className="product_row">
        <td>
            <img src={image} alt={name} width="300px" height="190px" />
        </td>
        <td>
            <p className="product_title">{name}</p>
        </td>
        <td>
            <p>Color: {color}</p>
        </td>
        <td>
            <p>${price}</p>
        </td>
        <div className='table_btns'>
        <td>
            {isAddedToCart ? (
            <Button className="card_btn" text="Remove from cart" onClick={onRemoveFromToCart} />
            ) : (
            <Button className="card_btn" text="Add to cart" onClick={onAddToCart} />
            )}
        </td>
        <td>
            {isAddedToFavourites ? (
            <StarSolid
                style={{ cursor: 'pointer' }}
                width={35}
                height={35}
                fill="#007eb9"
                onClick={onRemoveFromFavourite}
            />
            ) : (
            <StarNotSolid
                style={{ cursor: 'pointer' }}
                width={35}
                height={35}
                fill="#007eb9"
                onClick={onAddToFavourite}
            />
            )}
        </td>
        </div>
    </tr>
  );
}
