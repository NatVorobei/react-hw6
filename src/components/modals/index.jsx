import Button from "../buttons";
import '../modals/modal.scss';

export default function Modal(props) {
    if (!props.show){
        return null;
    }

    return (
        <>
            <div data-testid='overlay-test' className="overlay" onClick={props.onClose}></div>
            <div data-testid='modal-test' className="modal">
                <div className="modal_text">
                    <p>{props.text}</p>
                </div>

                <div className="modal_btns">
                    <Button className={"btn modal_btn"} text='Submit' onClick = {props.onSubmit}/>
                    <Button className={"btn modal_btn"} text='Cancel' onClick = {props.onClose}/>
                </div>
                
            </div>
        </>
    )
} 