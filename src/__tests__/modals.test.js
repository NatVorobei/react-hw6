import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import renderer from 'react-test-renderer';
import Modal from "../components/modals";

afterEach(cleanup);

test('should render correctly', () => {
    render(<Modal/>);
})

test('should render the modal when it is opened', () => {
    const testText = 'Modal Data';
    render(<Modal show={true} text={testText} onClose={jest.fn()} onSubmit={jest.fn()} />);
    const modalElement = screen.getByTestId('modal-test');
    expect(modalElement).toBeInTheDocument();
});

test('should not render the modal when it is not opened', () => {
    const testText = 'Modal Data';
    render(<Modal show={false} text={testText} onClose={jest.fn()} onSubmit={jest.fn()} />);
    const modalElement = screen.queryByTestId('modal-test');
    expect(modalElement).toBeNull();
});
  
test('should fire event onClose event when clicking on the overlay', () => {
    const testText = 'Modal Data';
    const onClose = jest.fn();
    render(<Modal show={true} text={testText} onClose={onClose} onSubmit={jest.fn()} />);
    const overlayElement = screen.getByTestId('overlay-test');
    fireEvent.click(overlayElement);
    expect(onClose).toHaveBeenCalledTimes(1);
});

test('should fire event onSubmit event when clicking on the Submit button', () => {
    const testText = 'Modal Data';
    const onSubmit = jest.fn();
    render(<Modal show={true} text={testText} onClose={jest.fn()} onSubmit={onSubmit} />);
    const submitButtonElement = screen.getByText('Submit');
    fireEvent.click(submitButtonElement);
    expect(onSubmit).toHaveBeenCalledTimes(1);
});

test('should render correctly when show is true', () => {
    const props = {
      show: true,
      text: 'Modal Data',
      onClose: jest.fn(),
      onSubmit: jest.fn(),
    };
    const component = renderer.create(<Modal {...props} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test('should render correctly when show is false', () => {
    const props = {
      show: false,
      text: 'Modal Data',
      onClose: jest.fn(),
      onSubmit: jest.fn(),
    };
    const component = renderer.create(<Modal {...props} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});