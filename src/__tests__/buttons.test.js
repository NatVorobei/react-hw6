import { render, screen, cleanup, fireEvent } from '@testing-library/react';
import Button from '../components/buttons';
import renderer from 'react-test-renderer';

afterEach(cleanup);
test('should render correctly', () => {
    render(<Button/>)
})

test('should render correctly by text', () => {
  const buttonText = 'Add to cart';
  render(<Button text={buttonText} />);
  const buttonElement = screen.getByText(buttonText);
  expect(buttonElement).toBeInTheDocument();
});

test('should add right class', () => {
    const className = 'card_btn';
    render(<Button text="Add to cart" className={className} />);
    const buttonElement = screen.getByText('Add to cart');
    expect(buttonElement).toHaveClass(className);
});

test('should fire event onClick', () => {
    const handleClick = jest.fn();
    render(<Button text="Add to cart" onClick={handleClick} />);
    const buttonElement = screen.getByText('Add to cart');
    fireEvent.click(buttonElement);
    expect(handleClick).toHaveBeenCalledTimes(1);
});

test('snapshot button', () => {
    const testButton = renderer.create(<Button/>).toJSON();
    expect(testButton).toMatchSnapshot();
})